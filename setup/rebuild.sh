# convience script to rebuild the code after a previous build

WORK_DIR=`pwd`

# determine appropriate directory for build/
SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
BUILDDIR=${SCRIPT_PATH%/*}/../../build/
echo "=== cd to `realpath $BUILDDIR` ==="
cd $BUILDDIR

echo "=== running make ==="
make -j8

echo "=== run x*/setup.sh ==="
source x*/setup.sh
cd $WORK_DIR
