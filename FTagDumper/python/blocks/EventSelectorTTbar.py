import os
from dataclasses import dataclass

from AnalysisAlgorithmsConfig.ConfigSequence import ConfigSequence
from AnalysisAlgorithmsConfig.ConfigAccumulator import ConfigAccumulator
from AnalysisAlgorithmsConfig.ConfigFactory import ConfigFactory

from .BaseBlock import BaseBlock

@dataclass
class EventSelectorTTbar(BaseBlock):
    ''''Performs event cleaning, pileup reweighting, object calibration, overlap
    removal, and a subsequent event selection.
    This is based on the TopCPToolkit, see `makeRecoConfiguration` in
    [TtbarCPalgoConfigBlocksAnalysis.py](https://gitlab.cern.ch/atlasphys-top/reco/TopCPToolkit/-/blob/main/source/TopCPToolkit/python/TtbarCPalgoConfigBlocksAnalysis.py)

    Note that this block can and should drop events from being being processed in the
    dumper algorithm, but processing can only be stopped when using `setMainAlgSeqToSeqAND`
    from [dumper.py](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/FTagDumper/python/dumper.py)

    Parameters
    ----------
    selections : dict[str, list[str]]
        A list of selection cuts to apply, as documented in the [TopCPToolkit](https://topcptoolkit.docs.cern.ch/settings/eventselection/)
        For example, we could add the following to our json config to do an e+mu ttbar
        selection:

        "selections": {
            "dilep": [
                "SUM_EL_N_MU_N 27000 >= 1",
                "EL_N 27000 == 1",
                "MU_N 27000 == 1",
                "JET_N 20000 >= 1",
                "JET_N 20000 >= 2",
                "EL_N 28000 == 1",
                "MU_N 28000 == 1",
                "JET_N 20000 == 2"
            ]
        }
    '''

    selections: dict[str, list[str]]

    def to_ca(self):
        config = ConfigSequence()
        factory = ConfigFactory()

        config += factory.makeConfig('CommonServices')
        config.setOptionValue('.runSystematics', False)

        ### event cleaning
        if not self.flags.Input.isMC:
            config += factory.makeConfig('EventCleaning')
            config.setOptionValue('.runEventCleaning', True)

        ### pileup reweighting
        config += factory.makeConfig('PileupReweighting')

        ### electrons
        config += factory.makeConfig(
            'Electrons', containerName='AnaElectrons'
        )
        config.setOptionValue('.crackVeto', True)

        config += factory.makeConfig(
            'Electrons.WorkingPoint',
            containerName='AnaElectrons',
            selectionName='loose'
        )
        config.setOptionValue('.identificationWP', "TightLH")
        config.setOptionValue('.isolationWP', "Tight_VarRad")

        config += factory.makeConfig (
            'Electrons.WorkingPoint',
            containerName='AnaElectrons',
            selectionName='tight'
        )
        config.setOptionValue('.identificationWP', "TightLH")
        config.setOptionValue('.isolationWP', "Tight_VarRad")

        config += factory.makeConfig('Electrons.PtEtaSelection', containerName='AnaElectrons')
        config.setOptionValue('.selectionDecoration', 'selectPtEta')
        config.setOptionValue('.minPt', 27e3)

        ### muons
        config += factory.makeConfig ('Muons', containerName='AnaMuons')

        config += factory.makeConfig(
            'Muons.WorkingPoint', containerName='AnaMuons', selectionName='loose'
        )
        config.setOptionValue('.quality', "Medium")
        config.setOptionValue('.isolation', "NonIso")

        config += factory.makeConfig(
            'Muons.WorkingPoint', containerName='AnaMuons', selectionName='tight'
        )
        config.setOptionValue('.quality', "Medium")
        config.setOptionValue('.isolation', "Tight_VarRad")

        config += factory.makeConfig('Muons.PtEtaSelection', containerName='AnaMuons')
        config.setOptionValue('.selectionDecoration', 'selectPtEta')
        config.setOptionValue('.minPt', 27e3)
        config.setOptionValue('.maxEta', 2.5)

        ### jets
        config += factory.makeConfig(
            'Jets', containerName='AnaJets', jetCollection='AntiKt4EMPFlowJets'
        )
        config.setOptionValue('.runGhostMuonAssociation', True)
        config.setOptionValue('.runNNJvtUpdate', True)

        config += factory.makeConfig('Jets.JVT', containerName='AnaJets')

        config += factory.makeConfig('Jets.PtEtaSelection', containerName='AnaJets')
        config.setOptionValue('.selectionDecoration', 'selectPtEta')
        config.setOptionValue('.minPt', 20e3)
        config.setOptionValue('.maxEta', 2.5)

        ### missing ET
        config += factory.makeConfig('MissingET', containerName='AnaMET')
        config.setOptionValue('.electrons', 'AnaElectrons.tight')
        config.setOptionValue('.muons', 'AnaMuons.tight')
        config.setOptionValue('.jets', 'AnaJets')
        config.setOptionValue('.photons', '')
        config.setOptionValue('.taus', '')

        ### overlap removal
        config += factory.makeConfig('OverlapRemoval')
        config.setOptionValue('.electrons', 'AnaElectrons.tight')
        config.setOptionValue('.muons', 'AnaMuons.tight')
        config.setOptionValue('.photons', '')
        config.setOptionValue('.jets', 'AnaJets.baselineJvt')
        config.setOptionValue('.taus', '')
        config.setOptionValue('.inputLabel', 'preselectOR')
        config.setOptionValue('.outputLabel', 'passesOR')

        ### event selection
        if (
            any(len(selection) > 0 for selection in self.selections.values())

            # don't select anythings when running a test, we most likely have an empty
            # output and the test will fail
            and not os.environ.get("DUMPSTER_CI_TEST") == "1"
        ):
            selection_cuts = {}
            for selection_name, selection in self.selections.items():
                selection_cuts[selection_name] = "\n".join(selection) + "\nSAVE\n"

            config += factory.makeConfig(
                'EventSelection',
                electrons='AnaElectrons.tight',
                muons='AnaMuons.tight',
                met='AnaMET',
                jets='AnaJets.baselineJvt',
                selectionCutsDict=selection_cuts,
            )

        config_accumulator = ConfigAccumulator(
            algSeq=None,
            autoconfigFromFlags=self.flags,
            noSysSuffix=True,
            noSystematics=True
        )
        config.fullConfigure(config_accumulator)
        return config_accumulator.CA
