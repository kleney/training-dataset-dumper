from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class ExampleDecorator(BaseBlock):
    '''Example starting point to add info to objects

    Parameters
    ----------
    decorator_value : float, optional
        The value to decorate to a jet
    target_collection : str, optional
        The name of the target jet collection to match to. If not
        provided, it will be taken from the dumper config.
    decorator_name : str
        Name of decoration to add
    '''
    decorator_value: str = None
    target_collection: str = None
    decorator_name: str = "exampleDecorator"

    def __post_init__(self):
        if self.target_collection is None:
            self.target_collection = self.dumper_config["jet_collection"]

    def to_ca(self):
        ca = ComponentAccumulator()
        opts = {}
        if self.decorator_value is not None:
            opts['decoratorValue'] = self.decorator_value
        target = f'{self.target_collection}.{self.decorator_name}'
        ca.addEventAlgo(
            CompFactory.ExampleDecoratorAlg(
                f'{self.target_collection}{self.decorator_name}Decorator',
                exampleDecorator=target,
                **opts,
            )
        )

        return ca
