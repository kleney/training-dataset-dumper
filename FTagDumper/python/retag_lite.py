"""
Lightweight setup for retagging with systematic variations
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from FTagDumper.trackUtil import applyTrackSys
from FTagDumper.jetUtil import applyJetSys

from AthenaConfiguration.ComponentFactory import CompFactory

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConfig import (
    InDetTrackTruthOriginToolCfg,
)
from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg,
)
from FlavorTagDiscriminants.FoldDecoratorConfig import FoldDecoratorCfg
from FlavorTagDiscriminants.FlavorTagNNConfig import addAndReturnSharingSvc

import copy, re, enum, json

try:
    from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
except ImportError:
    def BTagTrackAugmenterAlgCfg(*args, **kwargs):
        raise NotImplementedError(
            "No richman augmenter in your release, "
            "try again with Athena")


class FlipConfig(enum.Enum):
    STANDARD = ''
    SIMPLE_FLIP = 'SimpleFlip'
    FLIP_SIGN = 'Flip'
    NEGATIVE_IP_ONLY = 'Neg'


class IPPrefix(enum.Enum):
    poor = 'poormanIp_'
    rich = 'richmanIp_'


def dumpster_cfg(
        flags,
        output,
        config,
        force_full_precision=False
):
    ca = ComponentAccumulator()

    jet_collection = config['dumper']['jet_collection']

    # get an output group
    output = CompFactory.H5FileSvc(path=str(output))
    ca.addService(output)

    # add lightweight b-tagging
    #
    # TODO: also specify the name of the output tracks here, to ensure
    # that we can schedule several track semaring variations in
    # parallel too
    systs = dict(
        track_systs=config['track_systematics'],
        flip_configs=[FlipConfig[x] for x in config['flip_configs']],
        jet_systs=config['jet_systematics'],
        jet_sigma=config['jet_sigma'],
        ip_method=IPPrefix[config['ip_method']],
    )

    ca.merge(
        retagging(
            flags,
            track_collection=config['track_container'],
            decorate_track_truth=config['decorate_track_truth'],
            jet_collection=jet_collection,
            add_fold_hash=config['add_fold_hash'],
            **systs,
        )
    )

    # add dumper
    dumper_ext = '_'.join(
        systs['track_systs'] + systs['jet_systs'] +
        [cfg.name for cfg in systs['flip_configs'] + [systs['ip_method']]]
    )

    ca.addEventAlgo(
        CompFactory.JetDumperAlg(
            name=f'DatasetDumper{dumper_ext}',
            configJson=json.dumps(
                mungedConfig(config['dumper'], **systs)),
            output=output,
            forceFullPrecision=force_full_precision,
            group=dumper_ext
        )
    )
    return ca


def retagging(
        flags,
        jet_collection,
        track_collection="InDetTrackParticles",
        jet_systs=[],
        jet_sigma=1.0,
        track_systs=[],
        decorate_track_truth=False,
        flip_configs=[FlipConfig.STANDARD],
        ip_method=IPPrefix.poor,
        add_fold_hash=False,
):

    input_jets = jet_collection

    acc = ComponentAccumulator()

    output_jets = _jet_name(
        input_jets,
        jet_systematics=jet_systs)

    # In production we do the jet calibration _after_ we apply
    # tagging. This is a bit of a problem when we apply the jet
    # systematics as bottom up uncertainties, since they required a
    # calibrated jet collection.
    #
    # The solution is to introduce another systematic, called
    # 'PRODUCTION', which runs the tagging in the normal production
    # configuration.
    calibrate_jets_after_tagging = set(jet_systs) == {'PRODUCTION'}

    if calibrate_jets_after_tagging:
        # if we do the calibration after tagging we need another
        # intermediate collection to tag
        tagged_jets = f'{input_jets}_tagged'
    else:
        # otherwise we tag the output collection
        tagged_jets = output_jets

    acc.merge(
        applyJetSys(
            jet_systs,
            input_jets,
            jet_sigma,
            output_jet_collection=tagged_jets
        )
    )

    pvCol = "PrimaryVertices"
    if track_systs:
        varied_tracks = _track_name(track_systs)
        acc.merge(applyTrackSys(
            flags,
            track_systs,
            track_collection,
            jet=tagged_jets,
            output_tracks=varied_tracks
        ))
        track_collection = varied_tracks

    # The decorations need to be added after the systematics are
    # applied, in case there's some smearing which changes the d0
    # info.
    #
    if ip_method == IPPrefix.poor:
        # NOTE: this is NOT the same impact parameter we use in most
        # flavor tagging. It is a poor man's equivelent, which can run in
        # AthAnalysis and saves us a few minutes of setup time.
        #
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
                f'PoorMansIpAugmenterAlg_{track_collection}',
                trackContainer=track_collection,
                primaryVertexContainer=pvCol,
                prefix=ip_method.value,
            )
        )
    elif ip_method == IPPrefix.rich:
        # this is the rich-people method, it requires setup with full athena to run
        acc.merge(
            BTagTrackAugmenterAlgCfg(
                flags,
                TrackCollection=track_collection,
                PrimaryVertexCollectionName=pvCol,
                prefix=ip_method.value
            )
        )

    # add some truth labels
    if decorate_track_truth:
        trackTruthOriginTool = acc.popToolsAndMerge(
            InDetTrackTruthOriginToolCfg(flags)
        )
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
                'TruthParticleDecoratorAlg',
                trackTruthOriginTool=trackTruthOriginTool
            )
        )
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
                f'TrackTruthDecoratorAlg_{track_collection}',
                trackContainer=track_collection,
                trackTruthOriginTool=trackTruthOriginTool
            )
        )

    # now we assicoate the tracks to the jet
    tracks_on_jet = _track_name(track_systs)
    acc.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=tagged_jets,
        InputParticleCollection=track_collection,
        OutputParticleDecoration=tracks_on_jet,
    ))

    # Add jet fold decorator.
    #
    # This wasn't stored in the derivation I'm writing this for, but
    # it should be stored in the future. This block will liekly be
    # removable in the near future.
    if add_fold_hash:
        acc.merge(FoldDecoratorCfg(flags, jetCollection=tagged_jets))

    # Now we have to add an algorithm that tags the jets The input and
    # output remapping is handled via a map in the multifold tool.
    #
    variableRemapping = {
        'BTagTrackToJetAssociator': tracks_on_jet,
        'btagIp_': ip_method.value,
    }

    # add multifold GNN
    nn_base = 'BTagging/20231205/GN2v01/antikt4empflow'
    pf_nns = [f'{nn_base}/network_fold{n}.onnx' for n in range(4)]
    for fc in flip_configs:
        if fc.name == 'STANDARD':
            gnn_name = 'GN2v01'
        elif fc.name == 'SIMPLE_FLIP':
            gnn_name = 'GN2v01SimpleFlip'
        elif fc.name == 'FLIP_SIGN':
            gnn_name = 'GN2v01Flip'
        elif fc.name == 'NEGATIVE_IP_ONLY':
            gnn_name = 'GN2v01Neg'
        else:
            raise ValueError(f'Unknown flip config {fc.name}')
        auxVariableRemapping = {
            f'{gnn_name}_{x}' : f'GN2Alg_{fc.name}_{tagged_jets}_{track_collection}_{x}' for x in ['pb', 'pc', 'pu', 'ptau', 'TrackLinks', 'TrackOrigin', 'VertexIndex']
        }
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg(
                name=f'GN2Alg_{fc.name}_{tagged_jets}_{track_collection}',
                container=tagged_jets,
                constituentContainer=track_collection,
                decorator=CompFactory.FlavorTagDiscriminants.MultifoldGNNTool(
                    name=f'GN2Tool_{fc.name}',
                    foldHashName='jetFoldHash',
                    nnFiles=pf_nns,
                    variableRemapping={**variableRemapping, **auxVariableRemapping},
                    flipTagConfig=fc.name,
                    trackLinkType='IPARTICLE',
                    nnSharingService=addAndReturnSharingSvc(flags, acc)
                )
            )
        )

    if calibrate_jets_after_tagging:
        acc.merge(
            applyJetSys(
                sys_list=[],
                jet_collection=tagged_jets,
                jet_sigma=0,
                output_jet_collection=output_jets
            )
        )


    return acc


def mungedConfig(
        config_dict,
        track_systs,
        jet_systs,
        jet_sigma,
        flip_configs,
        ip_method
):
    """
    Return a modified version of the dumpster config, altered to support whatever systematics we're applying.
    """
    cd = config_dict
    nc = copy.deepcopy(cd)

    # rename the jet collection
    nc['jet_collection'] = _jet_name(
        cd['jet_collection'],
        jet_systematics=jet_systs)

    # rename the tracks
    if tracks := cd['tracks']:
        new_tracks = []
        for track_col in tracks:
            new_tracks.append(
                track_col | {
                    'input_name': _track_name(track_systs),
                    'ip_prefix': ip_method.value,
                }
            )
        nc['tracks'] = new_tracks

    # rename the outputs
    flip_re = re.compile('(GN[1-9]v[0-9]{2})_(p.*)')
    if floats := cd['variables'].get('jet',{}).get('floats',[]):
        for f in list(floats):
            if m := flip_re.match(f):
                floats.remove(f)
                for fc in flip_configs:
                    newval = '{}{}_{}'.format(
                        m.group(1),
                        fc.value,
                        m.group(2)
                    )
                    floats.append(newval)
        nc['variables']['jet']['floats'] = floats

    return nc


# renaming functions for collections
#
#
def _track_name(systematic_list):
    return _renamed('tracks', track_systematics=systematic_list)


def _jet_name(collection, jet_systematics):
    return _renamed(f'{collection}Copy', jet_systematics=jet_systematics)


def _renamed(collection, **systs):
    fullnames = [collection]
    for group in systs.values():
        for s in group:
            fullnames.append(s)
    return '_'.join(fullnames)
