# set up some grid programs

# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

# define functions
trap "_cleanup; trap - INT" INT # Note! using INT required for zsh
_cleanup() {
    unset -f _voms_proxy_long
    unset -f _cleanup
    echo "cleaning up"
}

if ! type voms-proxy-init &>/dev/null; then 
    # Note: better would be to use `lsetup emi` here, but that doesn't set
    # RUCIO_ACCOUNT which we use later to set the user's grid name. It's unclear
    # how rucio finds this, but if we find out we should move to emi instead.
    echo "voms-proxy-init not found, setting up rucio" >&2
    if ! lsetup rucio; then
        echo "problem with rucio setup" >&2
        return 2
    fi
fi 

_voms_proxy_long ()
{
    if ! type voms-proxy-info &> /dev/null; then
        echo "voms not set up!" 1>&2
        return 1
    fi
    local VOMS_ARGS="voms-proxy-init";
    if voms-proxy-info --exists --valid 24:00; then
        local TIME_LEFT=$(voms-proxy-info --timeleft);
        local HOURS=$(( $TIME_LEFT / 3600 ));
        local MINUTES=$(( $TIME_LEFT / 60 % 60 - 1 ));
        local NEW_TIME=$HOURS:$MINUTES;
        VOMS_ARGS+=" --voms atlas --noregen --valid $NEW_TIME";
    else
        # try setting up with FTAG production role
        VOMS_ARGS+=" --voms atlas:/atlas/perf-flavtag/Role=production"
        if ! voms-proxy-init $VOMS_ARGS --valid 96:00; then
            echo "User does not have permissions for perf-flavtag role, falling back to standard atlas role." 1>&2
            VOMS_ARGS="--voms atlas --valid 96:00";
            # define for use in grid-submit
            export HAS_FTAG_PERMISSIONS=false
        else
            # User has set up a proxy with FTAG productions, no need to regenerate
            VOMS_ARGS+=" --noregen"
            export HAS_FTAG_PERMISSIONS=true
        fi
    fi
    eval "voms-proxy-init $VOMS_ARGS"

}
    

if ! _voms_proxy_long; then
    if [[ ${1-x} == dry-run ]]; then
        echo "ignoring missing voms" 1>&2
        if [[ -z ${AtlasBuildStamp+set} ]]; then
            export AtlasBuildStamp=DUMMY_BUILD_STAMP
            echo "setting AtlasBuildStamp=${AtlasBuildStamp}" 1>&2
        fi
    else
        return 1
    fi
fi


if ! lsetup panda -q; then return 1; fi
if ! lsetup git -q; then return 1; fi
