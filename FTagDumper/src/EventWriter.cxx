#include "EventWriter.hh"
#include "EventOutputs.hh"
#include "EventWriterConfig.hh"
#include "HDF5Utils/Writer.h"

#include "add_event_info.hh"

EventWriter::EventWriter(
  H5::Group& g,
  const EventWriterConfig& c):
  m_writer(nullptr)
{
  using H5Utils::Compression;

  Compression f = Compression::STANDARD;
  Compression h = c.force_full_precision ?
    Compression::STANDARD : Compression::HALF_PRECISION;

  Writer::element_type::consumer_type vars;
  vars.add(
    "first_jet_index",
    [](const EventOutputs& o) { return o.first_jet_index; },
    0);
  vars.add(
    "n_jets",
    [](const EventOutputs& o) { return o.next_jet_index - o.first_jet_index; },
    0);
  add_event_info(vars, c.vars.customs, f);
  add_event_info(vars, c.vars.compressed, h);
  m_writer = std::make_unique<Writer::element_type>(g, c.name, vars);
}

EventWriter::~EventWriter() {
  m_writer->flush();
}

void EventWriter::write(const EventOutputs& o) {
  m_writer->fill(o);
}
