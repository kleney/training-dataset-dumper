#ifndef ADD_EVENT_INFO_HH
#define ADD_EVENT_INFO_HH

#include "xAODEventInfo/EventInfo.h"
#include "HDF5Utils/CompressionEnums.h"

#include <vector>
#include <string>
#include <regex>

template <typename T>
void add_event_info(
  T& vars,
  const std::vector<std::string>& keys,
  H5Utils::Compression compression = H5Utils::Compression::STANDARD)
{
  using InType = T::input_type;
  std::function<float(InType)> evt_weight = [](InType jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->mcEventWeight();
  };
  std::function<long long(InType)> evt_number = [](InType jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->eventNumber();
  };

  // store flag based on run number to indicate run2 / run3
  std::function<char(InType)> evt_is_run3 = [](InType jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return (jo.event_info->runNumber() > 410000);
  };

  std::function<float(InType)> evt_mu = [](InType jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->averageInteractionsPerCrossing();
  };

  std::function<float(InType)> evt_act_mu = [](InType jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->actualInteractionsPerCrossing();
  };
  std::regex int_type("^n[A-Z].*");

  for (const auto& key: keys) {
    if (key == "mcEventWeight") {
      vars.add(key, evt_weight, NAN, compression);
    } else if (key == "eventNumber") {
      vars.add(key, evt_number);
    } else if (key == "isRun3") {
      vars.add(key, evt_is_run3);
    } else if (key == "averageInteractionsPerCrossing") {
      vars.add(key, evt_mu, NAN, compression);
    } else if (key == "actualInteractionsPerCrossing") {
      vars.add(key, evt_act_mu, NAN, compression);
    } else if (std::regex_match(key, int_type)) {
      using IA = SG::AuxElement::ConstAccessor<int>;
      std::function getter([g=IA(key)] (InType jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter);
    } else {
      // let's just pretend everything else is a float
      using FA = SG::AuxElement::ConstAccessor<float>;
      std::function getter([g=FA(key)] (InType jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter, NAN, compression);
    }
  }
}





#endif // ADD_EVENT_INFO
