#ifndef ERROR_LOGGER_HH
#define ERROR_LOGGER_HH

#include "HDF5Utils/Writer.h"

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

struct LoggerInputs {
  const xAOD::Jet* jet;
  const xAOD::Jet* parent;
  size_t jet_rank;
  size_t missing_tracks;
};

using ErrorLogger = H5Utils::Writer<0,const LoggerInputs&>;

ErrorLogger getLogger(H5::Group&, std::string filename);

#endif
