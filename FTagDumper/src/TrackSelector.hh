#ifndef TRACK_SELECTOR_HH
#define TRACK_SELECTOR_HH

#include "xAODTracking/TrackParticleContainerFwd.h"
#include "xAODJet/JetFwd.h"

#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"

#include "TrackSelectorConfig.hh"

class TrackSelector
{
public:
  TrackSelector(TrackSelectorConfig = TrackSelectorConfig(),
                const std::string& link_name = "BTagTrackToJetAssociator");
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  Tracks get_tracks(const xAOD::Jet& jet) const;

private:
  typedef SG::AuxElement AE;
  typedef std::vector<ElementLink<xAOD::TrackParticleContainer> > TrackLinks;
  typedef std::vector<ElementLink<xAOD::IParticleContainer> > PartLinks;
  std::function<Tracks(const AE&)> m_track_associator;

  bool passed_cuts(const xAOD::TrackParticle &tp) const;
  TrackSelectorConfig::Cuts m_track_select_cfg;

  BTagTrackIpAccessor m_acc;
};

#endif
