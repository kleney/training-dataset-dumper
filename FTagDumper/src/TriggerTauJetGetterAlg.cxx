#include "TriggerTauJetGetterAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include "xAODJet/JetAuxContainer.h"
#include "xAODBTagging/BTaggingAuxContainer.h"

#include "xAODBTagging/BTaggingUtilities.h"

#include "AsgDataHandles/WriteDecorHandle.h"

#include <memory>


TriggerTauJetGetterAlg::TriggerTauJetGetterAlg(const std::string& name,
					       ISvcLocator* pSvcLocator):
  EL::AnaAlgorithm(name, pSvcLocator)
{
}



StatusCode TriggerTauJetGetterAlg::initialize() {
  // get handle to TrigDecisionTool
  ATH_CHECK(m_triggerDecision.retrieve());
  // retrieve jet modifiers
  ATH_CHECK(m_jetModifiers.retrieve());
  for (auto& handle: m_jetModifiers) {
    ATH_CHECK(handle.retrieve());
  }
  // initialize keys
  ATH_CHECK(m_trigTauJetKey.initialize());
  ATH_CHECK(m_outputJets.initialize());
  m_trackLinkKey = m_outputJets.key() + "." + m_trackLinkKey.key();
  ATH_CHECK(m_trackLinkKey.initialize());
  
  return StatusCode::SUCCESS;
}



StatusCode TriggerTauJetGetterAlg::execute () {

  // create WriteHandle for ouput Jet container
  const EventContext& context = Gaudi::Hive::currentContext();
  ATH_MSG_DEBUG("Recording jets");
  SG::WriteHandle outputJets(m_outputJets, context);
  ATH_CHECK(outputJets.record(
              std::make_unique<xAOD::JetContainer>(),
              std::make_unique<xAOD::JetAuxContainer>()));

  // check if primary chain and optional extra chains fired
  const std::string& chain = m_tauChain;
  if( m_triggerDecision->isPassed(chain) ){
    ATH_MSG_DEBUG("Primary chain passed " << chain);
    for (const std::string& add_chain: m_additionalChains) {
      if (!m_triggerDecision->isPassed(add_chain)) {
        ATH_MSG_DEBUG("Additional chain did not pass " << add_chain);
        return StatusCode::SUCCESS;
      } else {
        ATH_MSG_DEBUG("Additional chain passed " << add_chain);
      }
    }
  } else{
    ATH_MSG_DEBUG("Primary chain did not pass " << chain);
    setFilterPassed(false);
    return StatusCode::SUCCESS;
  }

  // retrieve TauJetContainer features for the primary chain
  auto taujets = m_triggerDecision->features<xAOD::TauJetContainer>(
    chain, TrigDefs::Physics, m_trigTauJetKey.key());
  ATH_MSG_DEBUG("Got " << taujets.size() << " tau jets");

  // create handle to define element links
  SG::WriteDecorHandle<xAOD::JetContainer, std::vector<ElementLink<xAOD::IParticleContainer> > > decHandle(m_trackLinkKey);

  // loop on TauJets
  for (const auto& taujet_link: taujets) {
 
    // get the TauJet object
    const xAOD::TauJet* trig_taujet = *taujet_link.link;

    // get the Jet object linked to the TauJet
    auto trig_jet = trig_taujet->jet();

    // push the jet in the output jet container
    *outputJets->emplace_back(new xAOD::Jet()) = *trig_jet;
    
    // optionally associate tracks from the TauJet
    if(m_doTrackMatching) {
      
      // loop on the TauTracks linked to the TauJet
      std::vector<ElementLink<xAOD::IParticleContainer> > matchedtracks;
      auto tautracks = trig_taujet->tracks();
      for (const auto& tautrack: tautracks) {
	// add each track to the vector of ElementLinks for this jet
	for (const auto& track_link: tautrack->trackLinks())
	  matchedtracks.push_back(track_link);
      }
      
      // establish ElementLink
      decHandle(*outputJets->back()) = matchedtracks;
      
    }

  }

  // final jet modifier call
  for (const auto& tool: m_jetModifiers) {
    ATH_CHECK(tool->modify(*outputJets));
  }
  return StatusCode::SUCCESS;
}



StatusCode TriggerTauJetGetterAlg::finalize () {
  return StatusCode::SUCCESS;
}
