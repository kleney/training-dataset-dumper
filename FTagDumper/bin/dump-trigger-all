#!/usr/bin/env python

"""
Dump some ftag trigger info!

This runs quite a few algorithms upstream of the dumping code, to
extract trigger jets from the trigger decisions, truth label them, and
compare them to offline reconstructed jets.
"""

import json
import sys

from AthenaConfiguration.ComponentFactory import CompFactory
from GaudiKernel.Constants import INFO

from FTagDumper import dumper
from FTagDumper import mctc
from FTagDumper import trigger as trig


def get_args():
    pflow_chain = 'HLT_j20_0eta290_020jvt_boffperf_pf_ftf_L1J15'
    emtopo_chain = 'HLT_j20_roiftf_preselj20_L1RD0_FILLED'
    dh = dict(help='(default: %(default)s)')
    parser = dumper.base_parser(__doc__)
    parser.add_argument('--pflow-chain', default=pflow_chain, **dh)
    parser.add_argument('--emtopo-chain', default=emtopo_chain, **dh)
    return parser.parse_args()

def run():
    args = get_args()


    flags = dumper.update_flags(args)
    flags.lock()

    # parse the configurations
    combined_cfg = dumper.combinedConfig(args.config_file)
    pf_cfg, pf_dumper_cfg = trig.getJobConfig(combined_cfg["pflow"])
    em_cfg, em_dumper_cfg = trig.getJobConfig(combined_cfg["emtopo"])

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = dumper.getMainConfig(flags)

    # force both chains to pass
    pf_cfg["additional_chains"] = pf_cfg["additional_chains"] + [args.emtopo_chain]
    em_cfg["additional_chains"] = em_cfg["additional_chains"] + [args.pflow_chain]

    ca.merge(trig.pflowDumper(
        flags,
        chain=args.pflow_chain,
        **pf_cfg
    ))
    ca.merge(trig.emtopoDumper(
        flags,
        chain=args.emtopo_chain,
        **em_cfg
    ))

    ca.merge(mctc.getMCTC())

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    ca.addEventAlgo(CompFactory.JetDumperAlg(
        'pflowDumper',
        output=output,
        configJson=json.dumps(pf_dumper_cfg),
        forceFullPrecision=args.force_full_precision,
        group='pflow'))
    ca.addEventAlgo(CompFactory.JetDumperAlg(
        'topoDumper',
        output=output,
        configJson=json.dumps(em_dumper_cfg),
        forceFullPrecision=args.force_full_precision,
        group='emtopo'))

    # Set Alg sequence to seqAND, no other algorithms shall be merged after
    dumper.setMainAlgSeqToSeqAND(ca)
    # print the configuration
    if flags.Exec.OutputLevel <= INFO:
        ca.printConfig(withDetails=True)

    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
